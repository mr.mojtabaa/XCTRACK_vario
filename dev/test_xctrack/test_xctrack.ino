#ifdef __AVR_ATtiny85__
#include <DigiCDC.h>
#define Serial SerialUSB
#define LED_BUILTIN PB1
#else
#define USB_MODE // uncomment for usb mode
#define BLUETOOTH_MODE // uncomment for bluetooth mode
#define USB_SPEED 115200 //define serial transmision speed
#define BLUETOOTH_SPEED 9600 //bluetooth speed (9600 by default)
#endif
#ifndef LED_BUILTIN
#define LED_BUILTIN 2
#endif

#ifdef BLUETOOTH_MODE
#ifdef ARDUINO_ARCH_ESP32
#include "BluetoothSerial.h"
BluetoothSerial BTserial;
#else
#include <SoftwareSerial.h>
#define RX 11 // not used (TX module bluetooth)
#define TX 12 // MISO on ISCP (RX module bluetooth)
SoftwareSerial BTserial(RX, TX); // RX not connected 
// Connect the HC-05 TX to Arduino pin 11 RX (MOSI). 
// Connect the HC-05 RX to Arduino pin 12 TX (MISO).
#endif
#endif

#define FREQUENCY 20 // freq output in Hz
#define PRESSURE 90000
#define DELTA 50

void setup() {       
  pinMode(LED_BUILTIN, OUTPUT);         
#ifdef __AVR_ATtiny85__ 
  Serial.begin();
  Serial.delay(10000); //long pause for xctrack
#else
#ifdef USB_MODE
  Serial.begin(USB_SPEED);       // set up arduino serial port
#endif
#ifdef BLUETOOTH_MODE
  BTserial.begin(BLUETOOTH_SPEED);     // start communication with the HC-05 using 38400
#endif
#endif
}

// the loop routine runs over and over again forever:
void loop() {
  static uint32_t get_time = millis();
  static uint32_t pressure = PRESSURE;
  static boolean dir = true;
  static uint8_t countLed = 0;
#ifdef __AVR_ATtiny85__
  Serial.delay(10);
#endif
#ifdef FREQUENCY
  if (millis() - get_time > (1000/FREQUENCY))
  {
    get_time = millis();
#endif
    if (dir) pressure += 1;
    else pressure -=1;
#if defined(__AVR_ATtiny85__) || defined(USB_MODE)
    Serial.print("PRS ");
    Serial.println(pressure,HEX);
#endif
#ifdef BLUETOOTH_MODE
    BTserial.print("PRS ");
    BTserial.println(pressure,HEX);
#endif
    if (pressure > PRESSURE+DELTA) dir = false;
    else if (pressure < PRESSURE) dir = true;
    if (countLed==0)
      digitalWrite(LED_BUILTIN, HIGH);
    else
      digitalWrite(LED_BUILTIN, LOW);
    countLed -= 2;
#ifdef FREQUENCY
  }
#endif
   /* DIGISPARK
   if you don't call a SerialUSB function (write, print, read, available, etc) 
   every 10ms or less then you must throw in some SerialUSB.refresh(); 
   for the USB to keep alive - also replace your delays - ie. delay(100); 
   with SerialUSB.delays ie. SerialUSB.delay(100);
   */
}