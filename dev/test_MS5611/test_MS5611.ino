#include "MS5611.h"
MS5611 sensor;

void setup() {
  Serial.begin(115200);
  Serial.println("checking MS5611 sensor...");

  if(!sensor.begin()) {
    sensor.debug();
    Serial.println("Reset in 2s...");
    delay(2000);
    setup();
  }
  Serial.println("sensor ok");
  sensor.debug();
}

void loop() {
  sensor.readOut();
  Serial.print("Temperature [Celcius]: ");
  Serial.println(sensor.getTemp()/100);
  Serial.print("Pressure [Pa]: ");
  Serial.println((uint32_t)(sensor.getPres()));
  Serial.println("---");
  delay(10000);
}