#include "HardwareSerial.h"
#include "Arduino.h"
#include "MS5611.h"

MS5611::MS5611(uint8_t aAddr){
    i2caddr=aAddr;
}

uint8_t MS5611::sendCmd(uint8_t aCMD)
{
  Wire.beginTransmission(i2caddr);
  Wire.write(aCMD);
  return Wire.endTransmission();
}

bool MS5611::begin() {
	Wire.begin();
	Wire.beginTransmission(i2caddr);
	uint8_t ret = Wire.endTransmission();
#ifndef __AVR_ATtiny85__
  if ( ret==0 ){
    bool rprom = readPROM();
    if( rprom ){
      return true;
    }else{
      lastError = ERROR_PROM;
      return false;
    }
  }
  else {
    lastError = ERROR_I2C;
    return false;
  }
#else
  return ret;
#endif
}

uint32_t MS5611::readADC(uint8_t aCMD)
{
  sendCmd(MS5611_CMD_ADC_CONV + aCMD); // start DAQ and conversion of ADC data
#ifndef __AVR_ATtiny85__
  switch (aCMD & 0x0f)
  {
    case MS5611_CMD_ADC_256 : delayMicroseconds(900);
    break;
    case MS5611_CMD_ADC_512 : delay(3);
    break;
    case MS5611_CMD_ADC_1024: delay(4);
    break;
    case MS5611_CMD_ADC_2048: delay(6);
    break;
    case MS5611_CMD_ADC_4096: delay(10);
    break;
  }
#else
  Serial.delay(10);
#endif
  sendCmd(MS5611_CMD_ADC_READ); // read out values
  Wire.requestFrom(i2caddr, 3);
  uint32_t adc = (uint32_t)Wire.read()<<16;
  adc += ((uint16_t)Wire.read()<<8);
  adc += Wire.read();
  return adc;
}

void MS5611::readOut(bool _debug) {
#ifndef TinyPrintRom
  uint32_t D2,D1;
  if (!_debug){
    D2 = readADC(MS5611_CMD_ADC_D2 + MS5611_CMD_ADC_4096);
    D1 = readADC(MS5611_CMD_ADC_D1 + MS5611_CMD_ADC_4096);
  }else{
    D1 = 8480966; D2 = 8658780;
    C[1]=44035;C[2]=43783;C[3]=28125;C[4]=25133;C[5]=33947;C[6]=29133;C[7]=3;
  } 
	// calculate 1st order pressure and temperature (MS5611 1st order algorithm)
  #ifdef __AVR_ATtiny85__
	double dT = D2-C5;
	double OFF = C2 + dT*C4;
	double SENS = C1 + dT*C3;
	T = 2000+(dT*C6);
  #else
	double dT = D2-(C[5]*pow(2,8));
	double OFF = (C[2]*pow(2,16)) + dT*C[4]/pow(2,7);
	double SENS = (C[1]*pow(2,15)) + dT*C[3]/pow(2,8);
	T = 2000+(dT*C[6]/pow(2,23));
	#endif 
	// perform higher order corrections
	if(T<2000) {
	  double T2 = dT*dT/pow(2,31);
	  double OFF2 = 5*(T-2000)*(T-2000)/pow(2,1);
	  double SENS2 = 5*(T-2000)*(T-2000)/pow(2,2);
	  if(T<-1500) {
	    OFF2 += 7*(T+1500)*(T+1500);
	    SENS2 += 11*(T+1500)*(T+1500)/pow(2,1);
	  }
    T -= T2;
    OFF -= OFF2;
    SENS -= SENS2;
  }

	P = (((D1*SENS)/pow(2,21)-OFF)/pow(2,15));
  
#endif
}

bool MS5611::readPROM() {
	sendCmd(MS5611_CMD_RESET);
	delay(3);
	for(uint8_t i=0;i<8;i++) 
	{
	    sendCmd(MS5611_CMD_PROM_RD+2*i);
	    Wire.requestFrom(i2caddr, 2);
	    C[i] = (uint16_t)Wire.read() << 8;
	    C[i] += Wire.read();
	}
  return CRC4(C);
}

double MS5611::getTemp() {
	return T;
}

double MS5611::getPres() {
	return P;
}

void MS5611::setI2Caddr(uint8_t aAddr) {
	i2caddr=aAddr;
}

uint8_t MS5611::getI2Caddr() {
	return i2caddr;
}

uint16_t MS5611::getPROM(uint8_t i){
  if (i >= sizeof(C) ) return 0;
  return C[i];
}

uint8_t MS5611::getLastError(){
  return lastError;
}

bool MS5611::scanI2C(){
  uint8_t count = 0;
  for(uint8_t address = 1; address < 127; address++ ) {
    Wire.beginTransmission(address);
    uint8_t error = Wire.endTransmission();
    if (error == 0)
    {
      i2cAdresses[count] = address;
      count++;
    }
  }
  foundI2C = count;
  return (count != 0);
}

bool MS5611::CRCTest(){
  uint16_t prom_test[8] = {0,44462,46297,28578,26888,33690,29306,4};
  return CRC4(prom_test);
}

bool MS5611::CRC4(uint16_t prom[])
{
    uint16_t n_rem = 0;  // CRC remainder
    uint16_t crc_backup = prom[7]; // save read CRC
    prom[7] = 0xFF00 & prom[7];   // CRC byte is replaced by 0
    for ( uint8_t cnt = 0; cnt < 16; cnt++ ) {   // operation is performed on bytes
      if ( cnt % 2 == 1 ) // choose LSB or MSB
        n_rem ^= (prom[cnt >> 1] & 0x00FF);
      else
        n_rem ^= (prom[cnt >> 1] >> 8);
      for ( uint8_t n_bit = 8; n_bit > 0; n_bit-- ) {
        if ( n_rem & 0x8000 )
          n_rem = (n_rem << 1) ^ 0x3000;
        else
            n_rem <<= 1 ;
      }
    }
    n_rem = (n_rem >> 12) & 0x000F; // final 4-bit remainder is CRC code
    prom[7] = crc_backup;
    return (n_rem == (prom[7] & 0x000F));
}

void MS5611::debug(){
  if (lastError==ERROR_I2C){
    Serial.println(MS5611_MESSAGE_ERROR_I2C);
    if (scanI2C()){
      Serial.print("found I2C devices :");
      for (uint8_t i=0; i<foundI2C;i++){
          Serial.print("0x");
          Serial.print(i2cAdresses[i],HEX);
          Serial.print(",");
      }
      Serial.println();
    }else{
      Serial.println("found no I2C devices");
    }
  }
  else if(lastError==ERROR_PROM){
    Serial.println(MS5611_MESSAGE_ERROR_PROM);
  } 

  // printing PROM
  Serial.print("PROM(C0->C8):");
  for (uint8_t i=0;i<7;i++){
    Serial.print(C[i]);Serial.print(",");
  }
  Serial.println(C[7]);

}