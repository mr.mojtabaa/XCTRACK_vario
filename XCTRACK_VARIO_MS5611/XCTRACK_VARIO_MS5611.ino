/*
VARIO XCTRACK
you can use :
- Arduino Nano
- or Digispark (with bootloader upgrade because of sketch space)
- 1 sensor ms5611
You have several choices for programming :
- usb mode or bluetooth mode (add bluetooth module for arduino)
- fastest mode or frequency mode
- lk8000 or prs sentence
default is bluetooth PRS 20 data/s

Normally you do not need USB_MODE if you use BLUETOOTH_MODE, just for debugging
*/
#ifndef __AVR_ATtiny85__
#define USB_MODE  // comment this line if you just want to use bluetooth
#define BLUETOOTH_MODE // this line for bluetooth mode
#endif
#define FREQUENCY 20  // freq output in Hz : uncomment this line and adjust value for frequency mode
#define USE_PRS //comment this line to use LK8000 sentence
/*
 these 2 sentences are available for xctrack : 
 LK8000 sentence : 32 chars to send
 $LK8EX1,pressure,altitude,vario,temperature,battery,*checksum

 PRS sentence : 10 chars to send
 PRS pressure in pascal in hexadecimal format
 
 bluetooth in 9600 bauds -> 1.25ms for a char
 LK8000 : 1.25*32=40ms
 PRS : 1.25*10=12.5ms
 USB in 115200 bauds -> 0.08ms for a char
 LK8000 : 0.08*32=2.5ms
 PRS : 0.08*10=0.8ms

 PRS is faster but LK8000 has checksum !
 PRS is more efficient with bluetooth
*/
#include "MS5611.h"
#ifndef ARDUINO_ARCH_ESP32
#include <avr/wdt.h>
#endif
MS5611 sensor;

/////////////////////////////////////////

#ifdef USB_MODE
#define USB_SPEED 115200  //serial transmision speed
#endif
#ifdef BLUETOOTH_MODE
#ifdef ARDUINO_ARCH_ESP32
#include "BluetoothSerial.h"
BluetoothSerial BTserial;
#else
#define RX 11                     // not used (TX module bluetooth)
#define TX 12                     // MISO on ISCP (RX module bluetooth)
#include <SoftwareSerial.h>
SoftwareSerial BTserial(RX, TX);  // RX not connected
#endif
#define BLUETOOTH_SPEED 9600      //bluetooth speed (9600 by default)
#endif

const char MESSAGE_VERSION[] = "XCTRACK VARIO V1.0.1";
const char MESSAGE_COMPILE_DATE[] = "compiled " __DATE__;
const char MESSAGE_CHECK[] = "Checking MS5611 sensor...";
const char MESSAGE_ERROR[] = "MS5611 error";
const char MESSAGE_RESET[] = "Reset in 1s...";
const char MESSAGE_BLUETOOTH[] = "Also Bluetooth mode";

void setup() {
#ifndef ARDUINO_ARCH_ESP32
  wdt_disable();
#endif
#ifdef __AVR_ATtiny85__
  Serial.begin();
  Serial.delay(10000);
#endif
#ifdef USB_MODE
  Serial.begin(USB_SPEED);
  Serial.println(MESSAGE_VERSION);
  Serial.println(MESSAGE_COMPILE_DATE);
  Serial.println(MESSAGE_CHECK);
#ifdef BLUETOOTH_MODE
  Serial.println(MESSAGE_BLUETOOTH);
#endif
#endif
#ifdef BLUETOOTH_MODE
  BTserial.begin(BLUETOOTH_SPEED);  // start communication with the HC-05 using 38400
  BTserial.println(MESSAGE_VERSION);
  BTserial.println(MESSAGE_COMPILE_DATE);
  BTserial.println(MESSAGE_CHECK);
#endif
#ifndef ARDUINO_ARCH_ESP32
  wdt_enable(WDTO_1S);  //enable the watchdog 1s without reset
#endif
  if (!sensor.begin()) {
#ifdef USB_MODE
    sensor.debug();
    Serial.println(MESSAGE_RESET);
#endif
#ifdef BLUETOOTH_MODE
    BTserial.println(MESSAGE_ERROR);
    BTserial.println(MESSAGE_RESET);
#endif
    delay(1200);  //for watchdog timeout
    setup();
  }
}

#ifdef FREQUENCY
uint32_t get_time = millis();
#endif
uint32_t Pressure = 0;
uint16_t n = 0;

void loop(void) {
#ifndef ARDUINO_ARCH_ESP32  
  wdt_reset();
#endif
  sensor.readOut();  // with OSR4096 takes about 10ms
  Pressure += sensor.getPres();
  n += 1;
#ifdef FREQUENCY
  if (millis() >= (get_time + (1000 / FREQUENCY)))
  {
    get_time = millis();
#endif
#if !defined(USE_PRS)  && !defined(__AVR_ATtiny85__)
    //https://github.com/LK8000/LK8000/blob/master/Docs/LK8EX1.txt
    //$LK8EX1,pressure,altitude,vario,temperature,battery,*checksum
    //$LK8EX1,pressure,99999,9999,temp,999,*checksum
    int8_t Temp = sensor.getTemp() / 100;
    char s[128];
    char format[]="LK8EX1,%lu,0,9999,%d,999,";
    snprintf(s,sizeof(s),format,Pressure/n,Temp);
    // Calculating checksum for data string
    uint16_t checksum = 0, bi;
    for (uint8_t ai = 0; ai < strlen(s); ai++) {
      bi = (uint8_t)s[ai];
      checksum ^= bi;
    }
    char s_checksum[128];
    snprintf(s_checksum,sizeof(s_checksum),"*%X",checksum);
    strcat(s,s_checksum);
    char str_out[128]="$";
    strcat(str_out,s);

#else  // use PRS sentence
    #ifdef __AVR_ATtiny85__
    Serial.print("PRS ");
    Serial.println(Pressure/n, HEX);
    #else
    char str_out[128];
    #ifdef ARDUINO_ARCH_ESP32
    snprintf(str_out, sizeof(str_out),"PRS %X",Pressure/n);
    #else
    snprintf(str_out, sizeof(str_out),"PRS %lX",Pressure/n);
    #endif
    #endif
#endif

#ifdef USB_MODE
    Serial.println(str_out);
#endif
#ifdef BLUETOOTH_MODE
    BTserial.println(str_out);
#endif
    Pressure = 0;
    n = 0;
#ifdef FREQUENCY
  }
#endif
}
